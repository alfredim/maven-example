# maven-example

Exercise

Create your first maven project

Add your favorite HTTP request library to the POM.xml

Make a Get request to:
http://api.icndb.com/jokes/random/?bridgeEndpoint=true

Return the random Chuck Norris joke as a String

Bonus point:

The response gets deserialized to JsonObject or Custom POJO

Only the Joke is returned in the response with no extra fields

Exercise 2

Install your jar file on your local maven repository

Create a new Maven project

Add the library created on the previous exercise to the POM.xml

Call the joke api and print the joke to the console

