package com.ikea.inter.model;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Value{

	@JsonProperty("id")
	private int id;

	@JsonProperty("categories")
	private List<Object> categories;

	@JsonProperty("joke")
	private String joke;

	public int getId(){
		return id;
	}

	public List<Object> getCategories(){
		return categories;
	}

	public String getJoke(){
		return joke;
	}
}