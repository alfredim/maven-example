package com.ikea.inter;

import static java.lang.System.*;
import static java.util.Objects.isNull;

public class Main {
    public static void main(String[] args) {
        JokeRequester jokeRequester = new JokeRequester();
        if(isNull(jokeRequester.getJoke())) {
            err.println("Unable to retrieve joke");
        } else {
            out.println(jokeRequester.getJoke());
        }
    }
}
