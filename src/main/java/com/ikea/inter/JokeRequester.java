package com.ikea.inter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ikea.inter.model.Joke;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.util.Objects;

public class JokeRequester {

    private final OkHttpClient client = new OkHttpClient();

    public String getJoke() {
        String url = "http://api.icndb.com/jokes/random/?bridgeEndpoint=true";

        ObjectMapper mapper = new ObjectMapper();
        Request request = new Request.Builder()
                .url(url)
                .build();

        try (Response response = client.newCall(request).execute()) {
            String jsonStr = Objects.requireNonNull(response.body()).string();
            Joke joke = mapper.readValue(jsonStr, Joke.class);
            return joke.getValue().getJoke().replace("&quot;", "\"");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
